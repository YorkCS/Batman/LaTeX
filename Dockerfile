FROM ubuntu:groovy

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y aspell aspell-en dot2tex gnuplot \
    graphviz make python3-pygments texlive-full && rm -rf /var/lib/apt/lists/*

COPY checkcites.lua /var/lib/
COPY texcount.pl /var/lib/
COPY checkcites.sh /usr/local/bin/
COPY texcount.sh /usr/local/bin/

RUN mkdir /data
WORKDIR /data

ENTRYPOINT ["make"]
