# LaTeX

## Prereqs

Any modern Linux or Mac running Docker 2018 CE or greater will suffice.

## Usage

The image will attempt to run your Makefile. Simply run:

```bash
$ docker run -ti -v ${PWD}:/data registry.gitlab.com/yorkcs/batman/latex
```

## Building

In standard Docker fashion:

```bash
$ docker build -t registry.gitlab.com/yorkcs/batman/latex:latest .
```

And then to publish:

```bash
$ docker push registry.gitlab.com/yorkcs/batman/latex
```
